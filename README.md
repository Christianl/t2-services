# Tarea 2 - Creacion de servicios
- [1.Objetivo](#objetivo)
- [2. Descripcion de contenedores](#descripcion_de_conetendores)
    - [2.1 Servidor](#servidor)
    - [2.2 Cliente](#cliente)
## objetivo

Ver los patrones generado entre dos software distintos que utilicen el mismo protocolo de modo cliente-servidor, el cual sera RESP que es el protocolo que redis usa y los software a utilizar para el servido sera redis y para el cliente sera node con la libreria ioredis.

## descripcion de contenedores

Para los contenedores se crearon imagenes especificas para cada uno, pero ambos funcionan bajo el sistemas operativo de ubuntu:20.04.

### servidor

Para el servidor se construira un Dockerfile donde se instalaran las dependencias necesarias para redis. Se instalara usando el codigo fuente, para esto se descargar usando `wget` para copiarlo y luego una vez dentro de la carpeta que lo que se hace es instalando usano `make`.

``` Dockerfile
# Selecionar la imagen donde se construira el software
FROM ubuntu:latest AS BUILD

# Configurar area local de la imagen para instalar las dependencias
RUN ln -snf /usr/share/zoneinfo/America/Santiago /etc/localtime && \
    echo America/Santiago > /etc/timezone

# Instalar las dependencias necesarias de redis
RUN apt-get update && \
    apt-get install -y build-essential wget pkg-config

# En que directorio dentro de la imagen que se usara
WORKDIR  /usr/app

# Primera linea: Descarga el codigo fuente del software
# Segunda linea descomprime el archivo tar.gz
# Tercera linea: Entra a la carpeta del software
# Cuarta linea: Procede a construir el software usando make
RUN wget https://download.redis.io/releases/redis-6.2.1.tar.gz && \ 
    tar -xzf redis-6.2.1.tar.gz && \
    cd redis-6.2.1 && \
    make 


#  Imagen Principal que contendra lo construido en la imagen anterior
FROM ubuntu:latest

# Se copia la carpeta donde se realizo la construccion del software redis a la imagen actual  a la carpeta /usr/redis
COPY --from=BUILD /usr/app/redis-6.2.1 /usr/redis
COPY redis.conf /usr/redis/redis.conf
WORKDIR /usr/redis

# Se agrega la carpeta donde contiene los archivos ejecutables de redis en la variable de entorno PATH
ENV PATH=$PATH:./src

# El comando principal de la image sera abrir el servidor de redis
CMD redis-server
```


### cliente

En el caso del cliente se uso este debido a que la imagen instalada desde el codigo fuente, tiende a demorarse mucho mas. Para esto se uso la imagen ya establecide de node la cual se usara la version 14.16.1 la cual estara montada con el sistema operativo de ubuntu.
```Dockerfile
# Imagen con la version de node estable
FROM node:14.16.1

# Directorio donde se trabajara 
WORKDIR /app

# instalar libreria para conectarse al servidor de redis
RUN npm install ioredis
```

El siguiente Dockerfile es la instalacion de la imagen con el codigo fuente, al probar los traficos entre los contenedores se utilizo esta imagen debido a que es lo que se pidio, pero ambas funcionan igual dado que son identicas.

```Dockerfile
FROM ubuntu:latest

RUN apt-get update && apt-get install -y python3 g++ make wget && \
    apt install python3-distutils

WORKDIR /var/node

RUN wget https://nodejs.org/dist/v14.16.1/node-v14.16.1.tar.gz && \
    tar -xzf node-v14.16.1.tar.gz && cd node-v14.16.1 \
    ./configure && make -j4 \
    make install

WORKDIR /app

ADD ./data commands
RUN npm install ioredis
```

## intrucciones

Para poder correr los servicios se ha usado el comando  `docker-compose`, con el cual ccontruira las imagenes de cada conentendor con sus respectivas configuraciones.

```docker
# Solamente la primera vez, dado que las imagenes no estan creadas
docker-compose up --build 

# Si ya estan creadas
docker-compose up 
```

Luego de tener activados ambos contenedores, los cuales se comunican dentro de una red distinta a la que esta por defecto en docker se entra al contenedor del cliente con el siguiente comando

```
docker exec -it redis-client bash
```

El comando anterior crea una interface donde uno pueda interactuar con el contenedor usando una shell como bash. Con esto dentro de la interfaz ejecutamos el comando de node lo cual nos creara una shell interactiva en el cual se cargara la libreria de _ioredis_ y se probaran los distintos patrones que se puedan encontrar.




